# A base java library

Base classes for java project with logger as class property and fluent validator. 

---
###Java code build notes

Requirements:

- Fluent validator library [bitbucket.org/airenas/aireno-java-validation-library](https://bitbucket.org/airenas/aireno-java-validation-library)
```bash
    git clone --branch 1.1 https://airenas@bitbucket.org/airenas/aireno-java-validation-library.git
    cd aireno-java-validation-library
    mvn install
    
```

2. Build, test and install the library using maven
```bash
    mvn install
```

---
### Author

**Airenas Vaičiūnas**

* [bitbucket.org/airenas](https://bitbucket.org/airenas)
* [linkedin.com/in/airenas](https://www.linkedin.com/in/airenas/)


---
### License

Copyright © 2019, [Airenas Vaičiūnas](https://bitbucket.org/airenas).
Released under the [The 3-Clause BSD License](LICENSE).

---
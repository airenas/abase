package com.aireno.base.utils;

import java.text.DecimalFormat;

/**
 * Exception utils functions.
 * @author Airenas Vaičiūnas
 * @since 13.11.9 (09.53)
 */
public class AExceptionUtils {
    /**
     * Collects exception messages in deep. max deep 5.
     * @param exc
     * @return
     */
    public static String getExceptionMessages(Throwable exc) {
        StringBuilder builder = new StringBuilder();
        colectExceptionMessages(exc, builder, 5);
        return builder.toString();
    }

    private static void colectExceptionMessages(Throwable exc, StringBuilder builder, int deep) {
        if (exc == null) {
            return;
        }
        if (deep < 1) {
            return;
        }
        if (builder.length() > 0) {
            if (builder.charAt(builder.length() - 1) != '.') {
                builder.append(". ");
            }
        }
        builder.append(exc.getMessage());
        colectExceptionMessages(exc.getCause(), builder, deep - 1);
    }
}

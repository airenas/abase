package com.aireno.base.utils;

import com.aireno.base.ClassBase;

public class StopWatch extends ClassBase implements AutoCloseable
{
    private final String name;
    private final long started;

    public StopWatch(String name)
    {
        this.name = name;
        this.started = System.currentTimeMillis();
        log(String.format("Started %s", name));
    }

    /**
     * Logs elapsed time since object construction
     */
    @Override
    public void close()
    {
        long ended = System.currentTimeMillis();
        log(String.format("%s - %s ms", name, ended - started));
    }

    private void log(String message)
    {
        getLog().debug(message);
    }
}
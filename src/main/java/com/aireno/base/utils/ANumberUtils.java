package com.aireno.base.utils;

import java.text.DecimalFormat;

/**
 * Number utils functions.
 * User: Airenas
 * Date: 13.2.13
 * Time: 23.48
 */
public class ANumberUtils {
    private static final long ONE_KB = 1024;
    private static final long ONE_MB = ONE_KB * ONE_KB;
    private static final long ONE_GB = ONE_MB * ONE_KB;


    public static int GetValueOrDefault(Integer item) {
        if (item != null) {
            return item.intValue();
        }
        return 0;
    }

    /**
     * return bytes display with kb, MB, using format 0,00
     *
     * @param size
     * @return
     */
    public static String bytesToDisplay(long size) {
        String displaySize;
        String format = "#.##";

        if (size / ONE_GB > 0) {
            displaySize = new DecimalFormat(format).format((float) size / ONE_GB) + " GB";
        } else if (size / ONE_MB > 0) {
            displaySize = new DecimalFormat(format).format((float) size / ONE_MB) + " MB";
        } else if (size / ONE_KB > 0) {
            displaySize = new DecimalFormat(format).format((float) size / ONE_KB) + " KB";
        } else {
            displaySize = String.valueOf(size) + " b";
        }
        return displaySize;
    }
}

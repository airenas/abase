package com.aireno.base.utils;

import java.io.File;
import java.io.IOException;

public class AFileUtils {
    public static boolean isFilenameValid(String file) {
        File f = new File(file);
        try {
            f.getCanonicalPath();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static String addPath(String modelDir, String wordsFile) {
        File file1 = new File(modelDir);
        File file2 = new File(file1, wordsFile);
        return file2.getPath();
        // return String.format("%s%s", modelDir, wordsFile);
    }
}
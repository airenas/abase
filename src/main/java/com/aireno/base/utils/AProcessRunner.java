package com.aireno.base.utils;

import com.aireno.base.ClassBase;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Helper class to execute command line
 *
 * @author Airenas Vaičiūnas
 *         <p/>
 * Date: 13.6.21
 * Time: 09.25
 */
public class AProcessRunner extends ClassBase {
    public void runProcess(String batFile, String dir, String... params) throws Exception {
        ProcessBuilder pb = init(batFile, dir, params);
        startAndWaitFor(pb);
    }

    public ProcessBuilder init(String batFile, String dir, String... params) throws Exception {
        getLog().debug("Call process '{}', dir '{}'", batFile, dir);
        ProcessBuilder pb = new ProcessBuilder(batFile);
        if (params != null) {
            for (String item : params) {
                getLog().debug("add param '{}'", item);
                pb.command().add(item);
            }
        }
        if (dir != null) {
           pb.directory(new File(dir));
        }
        return pb;
    }

    public void startAndWaitFor(ProcessBuilder pb) throws Exception {
        Process p = pb.start();
        BufferedReader err = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        int result = p.waitFor();
        String error = readBuffer(err);
        getValidator().that(error).as("Error: %s").isEmpty();
        getValidator().that(result).as("Error returned %s from process").isEqualTo(0);
        // maybe returned 0, but error in stderr?

        getLog().debug("call done");
    }

    public Process runProcessAsync(String batFile, String dir, String... params) throws Exception {
        ProcessBuilder pb = init(batFile, dir, params);
        Process p = pb.start();
        getLog().debug("call done");
        return p;
    }

    private String readBuffer(BufferedReader err) throws IOException {
        StringBuilder builder = new StringBuilder();
        String aux;
        while ((aux = err.readLine()) != null) {
            builder.append(aux);
        }

        return builder.toString();
    }
}

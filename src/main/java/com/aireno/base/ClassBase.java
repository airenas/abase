package com.aireno.base;

import com.aireno.avalid.DefaultExceptionProvider;
import com.aireno.avalid.DefaultValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created with IntelliJ IDEA.
 * User: Airenas
 * Date: 13.2.11
 * Time: 21.41
 */
public abstract class ClassBase {

    private Logger logger;

    protected Logger getLog() {
        if (logger == null) {
            logger = LoggerFactory.getLogger(getClass());
        }
        return logger;
    }

    protected DefaultValidator getValidator() {
        return new DefaultValidator(new DefaultExceptionProvider());
    }
}

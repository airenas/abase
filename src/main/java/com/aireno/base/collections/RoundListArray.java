package com.aireno.base.collections;

/**
 * Base round up list array
 *
 * @author Airenas.
 *         Date: 13.7.27
 *         Time: 14.06
 */
public abstract class RoundListArray implements RoundList {
    // size of the real array
    protected int realSize;
    // logical pos of starting array index
    protected int firstIndex;
    // logical size
    protected int size;

    public RoundListArray(int realSize) {
        this.realSize = realSize;
        size = 0;
    }

    public void clear() {
        size = 0;
        firstIndex = 0;
        clearData();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int firstIndex() {
        return firstIndex;
    }

    @Override
    public int realSize() {
        return size - firstIndex;
    }

    /**
     * set data to zero
     */
    protected abstract void clearData();

    protected void setSize(int index) {
        if (index < firstIndex) {
            throw new ArrayIndexOutOfBoundsException(index);
        }
        if (index >= size) {
            size = index + 1;
            if (size > realSize) {
                firstIndex = size - realSize;
            }
        }
    }

    protected int getRealIndex(int index) {
        if (index < firstIndex || index >= size) {
            throw new ArrayIndexOutOfBoundsException(index);
        }
        int realIndex = index % realSize;
        return realIndex;
    }
}

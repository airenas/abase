package com.aireno.base.collections;

/**
 * Bytes round up list array
 *
 * @author Airenas Vaičiūnas
 * @since 13.7.27 (14.06)
 */
public class ByteRoundListArray extends RoundListArray implements ByteRoundList {
    byte[] array;

    public ByteRoundListArray(int maxSize) {
        super(maxSize);
        array = new byte[maxSize];
    }

    @Override
    protected void clearData() {
        for (int i = 0; i < realSize; i++) {
            array[i] = 0;
        }
    }

    @Override
    public byte get(int pos) {
        int realPos = getRealIndex(pos);
        return array[realPos];
    }

    @Override
    public void add(byte value) {
        int index = size;
        setSize(index);
        int realPos = getRealIndex(index);
        array[realPos] = value;
    }

    @Override
    public void set(int pos, byte value) {
        setSize(pos);
        int realPos = getRealIndex(pos);
        array[realPos] = value;
    }

    @Override
    public synchronized void write(byte[] b, int off, int len) {
        for (int i = 0; i < len; i++) {
            add(b[i]);
        }
    }

    public byte[] copyRange(int from, int to) {
        int newLength = to - from;
        if (newLength < 0)
            throw new IllegalArgumentException(from + " > " + to);
        byte[] copy = new byte[newLength];
        for (int i = 0; i < newLength; i++) {
            copy[i] = get(i + from);
        }
        return copy;
    }

    public byte[] toByteArray() {
        return copyRange(firstIndex, size);
    }
}

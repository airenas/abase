package com.aireno.base.collections;

/**
 * Float round up list array
 *
 * @author Airenas.
 *         Date: 13.7.27
 *         Time: 14.06
 */
public class FloatRoundListArray extends RoundListArray implements FloatRoundList {
    float[] array;

    public FloatRoundListArray(int maxSize) {
        super(maxSize);
        array = new float[maxSize];
    }

    @Override
    protected void clearData() {
        for (int i = 0; i < realSize; i++) {
            array[i] = 0;
        }
    }

    @Override
    public float get(int pos) {
        int realPos = getRealIndex(pos);
        return array[realPos];
    }

    @Override
    public void add(float value) {
        int index = size;
        setSize(index);
        int realPos = getRealIndex(index);
        array[realPos] = value;
    }

    @Override
    public void set(int pos, float value) {
        setSize(pos);
        int realPos = getRealIndex(pos);
        array[realPos] = value;
    }
}

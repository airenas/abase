package com.aireno.base.collections;

/**
 * Short round up list array
 *
 * @author Airenas Vaičiūnas
* @since 13.10.11 (10.08)
 */
public class ShortRoundListArray extends RoundListArray implements ShortRoundList {
    short[] array;

    public ShortRoundListArray(int maxSize) {
        super(maxSize);
        array = new short[maxSize];
    }

    @Override
    protected void clearData() {
        for (int i = 0; i < realSize; i++) {
            array[i] = 0;
        }
    }

    @Override
    public short get(int pos) {
        int realPos = getRealIndex(pos);
        return array[realPos];
    }

    @Override
    public void add(short value) {
        int index = size;
        setSize(index);
        int realPos = getRealIndex(index);
        array[realPos] = value;
    }

    @Override
    public void set(int pos, short value) {
        setSize(pos);
        int realPos = getRealIndex(pos);
        array[realPos] = value;
    }
}

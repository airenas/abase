package com.aireno.base.collections;

/**
 * Byte array round up list interface
 *
 * @author Airenas.
 *         Date: 13.7.27
 *         Time: 14.06
 */
public interface ByteRoundList extends RoundList {
    byte get(int pos);

    void add(byte value);

    void set(int pos, byte value);

    void write(byte[] b, int off, int len);
}

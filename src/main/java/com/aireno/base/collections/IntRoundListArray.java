package com.aireno.base.collections;

/**
 * Int round up list array
 *
 * @author Airenas Vaičiūnas
* @since 13.10.11 (10.08)
 */
public class IntRoundListArray extends RoundListArray implements IntRoundList {
    int[] array;

    public IntRoundListArray(int maxSize) {
        super(maxSize);
        array = new int[maxSize];
    }

    @Override
    protected void clearData() {
        for (int i = 0; i < realSize; i++) {
            array[i] = 0;
        }
    }

    @Override
    public int get(int pos) {
        int realPos = getRealIndex(pos);
        return array[realPos];
    }

    @Override
    public void add(int value) {
        int index = size;
        setSize(index);
        int realPos = getRealIndex(index);
        array[realPos] = value;
    }

    @Override
    public void set(int pos, int value) {
        setSize(pos);
        int realPos = getRealIndex(pos);
        array[realPos] = value;
    }
}

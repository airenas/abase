package com.aireno.base.collections;

/**
 * @author Airenas Vaiciunas.
 * @since 13.10.11 (10.07)
 */
public interface IntRoundList extends RoundList {
    int get(int pos);
    void add(int value);
    void set(int pos, int value);
}

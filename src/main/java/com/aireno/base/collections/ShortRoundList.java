package com.aireno.base.collections;

/**
 * @author Airenas Vaiciunas.
 * @since 13.10.11 (10.07)
 */
public interface ShortRoundList extends RoundList {
    short get(int pos);
    void add(short value);
    void set(int pos, short value);
}

package com.aireno.base.collections;

/**
 * Float round up list interface
 *
 * @author Airenas.
 *         Date: 13.7.27
 *         Time: 14.06
 */
public interface FloatRoundList extends RoundList {
    float get(int pos);
    void add(float value);
    void set(int pos, float value);
}

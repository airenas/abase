package com.aireno.base.collections;

/**
 * Base round up list interface
 *
 * @author Airenas.
 *         Date: 13.7.27
 *         Time: 14.06
 */
public interface RoundList {
    int size();
    int firstIndex();
    void clear();
    int realSize();
}

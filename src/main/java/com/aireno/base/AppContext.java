package com.aireno.base;

/**
 * Created with IntelliJ IDEA.
 * User: Airenas
 * Date: 13.2.11
 * Time: 21.40

 */
public interface AppContext {
    public <T> T getObject(Class<T> tClass) throws Exception;
}

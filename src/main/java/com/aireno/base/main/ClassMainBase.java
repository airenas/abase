package com.aireno.base.main;

import com.aireno.base.ClassBase;
import com.aireno.base.settings.ASettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


/**
 * @author Airenas Vaičiūnas
 * @since 2015.02.11 (22:05)
 */
public abstract class ClassMainBase extends ClassBase{

    private ASettings settings;

    protected void run(String ... args){
        getLog().info("Starting " + getClass().getSimpleName());
        try {
            runInternal(args);
        } catch (Exception e) {
            getLog().error("Error", e);
        }
        getLog().info("Stopped " + getClass().getSimpleName());
    }

    protected abstract void runInternal(String[] args) throws Exception;

    protected ASettings getSettings() {
        if (settings == null) {
            try {
                settings = loadSettings();
            } catch (IOException e) {
                getLog().error(String.format("Cannot load properties from file '%s'",
                        getPropertiesFile().getAbsoluteFile()), e);
            }
        }
        return settings;
    }

    protected ASettings loadSettings() throws IOException {
        ASettings result = new ASettings();
        //result.load(new FileInputStream(getPropertiesFile()));
        return result;
    }

    public File getPropertiesFile() {
        return new File(String.format("%s.config", getClass().getSimpleName()));
    }
}

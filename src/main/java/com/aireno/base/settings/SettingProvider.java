package com.aireno.base.settings;

/**
 * @author Airenas Vaičiūnas
 * @since 10/20/15
 */
public interface SettingProvider{
    String getNotNullString(String key);
    String getString(String key, String sDefault);
    String getString(String key);
}

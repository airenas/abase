package com.aireno.base.settings;

import com.aireno.base.ClassBase;

/**
 * @author Airenas Vaičiūnas
 * @since 10/20/15
 */
public class EnvironmentSettingProvider extends ClassBase implements SettingProvider {

    public String getNotNullString(String key) {
        String result = getString(key);
        getValidator().that(result).as("No system environment variable '%s'", key).isNotNull();
        return result;

    }

    public String getString(String key, String sDefault) {
        String result = getString(key);
        if (result == null) {
            return sDefault;
        }
        return result;
    }

    public String getString(String key) {
        return System.getenv().get(key);
    }
}

package com.aireno.base.settings;

import com.aireno.avalid.AValid;

import java.util.Properties;

/**
 * @author Airenas Vaiciunas.
 *         2015.02.11 22:32
 */
public class ASettings extends Properties{
    public String getNonNullProperty(String key) {
        String property = getProperty(key);
        AValid.validate().that(property).as("No key '%s'", key).isNotNull();
        return property;
    }

    @Override
    public String getProperty(String key) {
        String property = System.getProperty(key);
        if (property == null) {
            property = super.getProperty(key);
        }
        return property;
    }
}

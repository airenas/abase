package com.aireno.base.collections;

import com.aireno.base.BaseTest;
import org.junit.Assert;
import org.junit.Test;

/**
 * Base round up list array
 *
 * @author Airenas.
 *         Date: 13.7.27
 *         Time: 14.06
 */
public class ByteRoundListArrayTest extends BaseTest {

    @Test
    public void TestArrayValues() {
        ByteRoundListArray array = new ByteRoundListArray(10);
        for (int i = 0; i < 100; i++) {
            array.set(i, (byte)i);
            byte value = array.get(i);
            Assert.assertEquals((byte) i, value);
        }
    }

    @Test
    public void TestArrayAddValues() {
        int size = 10;
        ByteRoundListArray array = new ByteRoundListArray(size);
        for (int i = 0; i < size; i++) {
            array.add((byte)i);
        }

        for (int i = 0; i < size; i++) {
            byte value = array.get(i);
            Assert.assertEquals((byte) i, value);
        }
    }

    @Test
    public void TestArrayPerformance() {
        int size = 10;
        ByteRoundListArray array = new ByteRoundListArray(size);
        getLog().info("starting");
        for (int i = 0; i < 10000000; i++) {
            Assert.assertEquals(array.realSize(), Math.min(i, size));
            Assert.assertEquals(array.size(), i);
            array.set(i, (byte)(i % 128));
            int value = array.get(i);
            Assert.assertEquals (i % 128, value);
        }
        getLog().info("finished");
    }

    @Test
    public void TestBigArrayPerformance() {
        int size = 100000;
        ByteRoundListArray array = new ByteRoundListArray(size);
        getLog().info("starting");
        for (int i = 0; i < 10000000; i++) {
            Assert.assertEquals(array.realSize(), Math.min(i, size));
            Assert.assertEquals(array.size(), i);
            array.set(i, (byte)(i % 128));
            int value = array.get(i);
            Assert.assertEquals (i % 128, value);
        }
        getLog().info("finished");
    }
}

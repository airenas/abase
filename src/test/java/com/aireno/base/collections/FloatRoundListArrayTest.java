package com.aireno.base.collections;

import com.aireno.base.BaseTest;
import org.junit.Assert;
import org.junit.Test;

/**
 * Base round up list array
 *
 * @author Airenas.
 *         Date: 13.7.27
 *         Time: 14.06
 */
public class FloatRoundListArrayTest extends BaseTest {

    @Test
    public void TestArrayValues() {
        FloatRoundListArray array = new FloatRoundListArray(10);
        for (int i = 0; i < 100; i++) {
            array.set(i, i);
            float value = array.get(i);
            Assert.assertEquals((float) i, value, 0.000000001);
        }
    }

    @Test
    public void TestArrayAddValues() {
        int size = 10;
        FloatRoundListArray array = new FloatRoundListArray(size);
        for (int i = 0; i < size; i++) {
            array.add(i);
        }

        for (int i = 0; i < size; i++) {
            float value = array.get(i);
            Assert.assertEquals((float) i, value, 0.000000001);
        }
    }

    @Test
    public void TestArraySize() {
        int size = 10;
        FloatRoundListArray array = new FloatRoundListArray(size);
        for (int i = 0; i < 100; i++) {
            Assert.assertEquals(array.realSize(), Math.min(i, size));
            Assert.assertEquals(array.size(), i);
            array.set(i, i);
            Assert.assertEquals(array.realSize(), Math.min(i + 1, size));
            Assert.assertEquals(array.size(), i + 1);
        }
    }

    @Test
    public void TestArrayPerformance() {
        int size = 10;
        FloatRoundListArray array = new FloatRoundListArray(size);
        getLog().info("starting");
        for (int i = 0; i < 10000000; i++) {
            Assert.assertEquals(array.realSize(), Math.min(i, size));
            Assert.assertEquals(array.size(), i);
            array.set(i, i);
            float value = array.get(i);
            Assert.assertEquals((float) i, value, 0.000000001);
        }
        getLog().info("finished");
    }

    @Test
    public void TestBigArrayPerformance() {
        int size = 100000;
        FloatRoundListArray array = new FloatRoundListArray(size);
        getLog().info("starting");
        for (int i = 0; i < 10000000; i++) {
            Assert.assertEquals(array.realSize(), Math.min(i, size));
            Assert.assertEquals(array.size(), i);
            array.set(i, i);
            float value = array.get(i);
            Assert.assertEquals((float) i, value, 0.000000001);
        }
        getLog().info("finished");
    }

    @Test
    public void TestClear() {
        int size = 100000;
        FloatRoundListArray array = new FloatRoundListArray(size);
        getLog().info("starting");
        for (int i = 0; i < size; i++) {
            Assert.assertEquals(array.realSize(), Math.min(i, size));
            Assert.assertEquals(array.size(), i);
            array.set(i, i);
            float value = array.get(i);
            Assert.assertEquals((float) i, value, 0.000000001);
        }

        for (int i = 0; i < size; i++) {
            float value = array.get(i);
            Assert.assertEquals((float) i, value, 0.000000001);
        }

        array.clear();
        Assert.assertEquals(0, array.size());
        getLog().info("finished");
    }
}
